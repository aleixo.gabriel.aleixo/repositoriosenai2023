const { createApp } = Vue;

createApp({
    data() {
        return {
        valorDisplay: "0",
        operador: null,
        numeroAtual: null,
        numeroAnterior: null,
        resultadoCalculado: false,
        tamanhoLetra: 50 + "px",
        //login
        usuario: '',
        senha: '',
        erro: null,
        sucesso: null,
        //arrays(vetores)paraArmazenamentoDosNomesDeUsuariosESenhas
        usuarios: ["admin", "Aleixo", "Nome2"],
        senhas: ["senhaAdm", "senha", "Senha2"],
        userAdmin: false,
        mostrarEntrada: false,
        //variaveis para tratamento das informações dos novos usuários
        newUsername: "",
        newPassword: "",
        confirmPassword: "",
        mostrarLista: false,
        };
    },
    methods: {
        login(){
            // alert("testando...");
            //Simulando Uma Requisição De Login Assicrona
            setTimeout(() => {
                if((this.usuario === "GaAleixo" && this.senha === "senhaSegura") || 
                (this.usuario === "nome" && this.senha === "senha") ||
                (this.usuario === "bomDia" && this.senha === "boaNoite") ||
                (this.usuario === "abacate" && this.senha === "vitamina")
                ){
                    setTimeout(function(){
                        window.location.href='calculadora.html'
                    }, 3000);
                    this.erro = null
                    this.sucesso = "Login efetuado com sucesso!";
                    // alert("Login efetuado com sucesso!")
                }//fIf
                else{
                    // alert("Usuário ou Senha incorretos!")
                    this.erro = "Usuário ou Senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);
        },//fLogin
        login2(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true
                
                //verificação de usuário e senha cadastrado ou não nos arrays
                const index = this.usuarios.indexOf(this.usuario);
                if(index !== -1 && this.senhas[index] === this.senha){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    // registrando o usuário no localStorage para lembretes de acessos
                    localStorage.setItem("usuario", this.usuario);
                    localStorage.setItem("senha", this.senha);

                    //verificando se o usuário é adm
                    if(this.usuario === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADM!";
                    }
                }//fIf
                else{
                    this.sucesso = null;
                    this.erro = "Usuário e/ou senha incorretos!";
                }   
            }, 1000);            
        },//fLogin2
        paginaCadastro(){
            this.mostrarEntrada = false;
            // if(this.userAdmin == true){
                this.erro = null;
                this.sucesso = "Carregando página de Cadastro...";
                this.mostrarEntrada = true;
                //espera estratégica antes do carregamento da página
                setTimeout(() => {}, 2000);
                setTimeout(() => {
                    window.location.href = "cadastro.html";
                }, 1000);
            // }
            // else{
            //     this.sucesso = null
            //     setTimeout(() => {
            //         this.mostrarEntrada = true
            //         this.erro = "Sem privilégios ADM!!!";
            //     }, 1000);
            // }
        },//fPaginaCadastro
        adicionarUsuario(){
            this.mostrarEntrada = false;
            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem ("senha");

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.usuario === "admin"){
                    //verificando se o novo usuário é diferente de vazio e se ele já não está cadastrado no sistema
                    if(!this.usuarios.includes(this.newUsername) && this.newUsername !== "" && !this.newUsername.includes(" ")){
                        //Validação da senha digitada para cadastro no array
                        if(this.newPassword !== "" && !this.newPassword.includes(" ") && this.newPassword === this.confirmPassword){
                            // inserindo o novo usuário e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);
                            // armazenando o usuario recem cadastrado no localStorage
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));
                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                            this.sucesso = "Cadastrado com sucesso";
                            this.erro = null
                        }//fIfPasswort
                        else{
                            this.erro = "Senha inválida"
                            this.sucesso = null
                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                        }//fElsePassword
                    }//fIfIncludes
                    else{
                        this.erro = "Usuário inválido";
                        this.sucesso = null;
                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                    }//fElseIncludes
                }//fIfAdm
                else{
                    this.erro = "Não está logado como Adm";
                    this.sucesso = null;
                    this.newUsername = "";
                    this.newPassword = "";
                    this.confirmPassword = "";
                }//fElseAdm
            }, 750);
        },
        getNumero(numero) {
            this.ajusteTamanhoDisplay();
            // ajusteTamanhoDisplay();
            if (this.resultadoCalculado) {
                this.valorDisplay = numero.toString();
                this.resultadoCalculado = false;
            } else if (this.valorDisplay === "0") {
                this.valorDisplay = numero.toString();
            } else {
                this.valorDisplay += numero.toString();
            }
        },//fGetNumero
        apagar() {
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.resultadoCalculado = false;
            this.tamanhoLetra = 50 + "px"
        },//fApagar
        decimal() {
            if (!this.valorDisplay.includes(".")) {
                this.valorDisplay += ".";
            }
        },//fDecimal
        operacoes(operacao) {
            // if(this.valorDisplay.content("op"))
            if (this.numeroAtual !== null) {
                const displayAtual = parseFloat(this.valorDisplay);
                if (this.operador !== null) {
                        switch (this.operador) {
                            case "+":
                                this.valorDisplay = ((this.numeroAtual + displayAtual ).toFixed(5)* 1).toString();
                                break;
                            case "-":
                                this.valorDisplay = ((this.numeroAtual - displayAtual ).toFixed(5)* 1).toString();                
                                break;
                            case "/":
                                this.valorDisplay = ((this.numeroAtual / displayAtual ).toFixed(5)* 1).toString();                
                                break;
                            case "*":
                                this.valorDisplay = ((this.numeroAtual * displayAtual ).toFixed(5)* 1).toString();
                                break;
                        }
                        this.numeroAnterior = this.numeroAtual;
                        this.numeroAtual = null;
                        this.operador = null;
                        if (this.valorDisplay === "Infinity") {
                            this.valorDisplay = "Impossivel";
                        }
                        if (this.valorDisplay === "NaN") {
                            this.valorDisplay = "Impossivel";
                        }
                        // if(this.valorDisplay.includes(".")){
                        // const numDecimal = parseFloat(this.valorDisplay);
                        // this.valorDisplay = (numDecimal.toFixed(2)).toString;
                } 
                else {
                    this.numeroAnterior = displayAtual;
                }
            }
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if (this.operador !== "=") {
                this.valorDisplay = "0";
            } 
            else {
                this.resultadoCalculado = true;
            }
            this.ajusteTamanhoDisplay();
        },//fOperecoes
        ajusteTamanhoDisplay(){
            if(this.valorDisplay.length >= 15){
                this.tamanhoLetra = 10 + "px"
            }
            else if(this.valorDisplay.length >= 10){
                this.tamanhoLetra = 20 + "px"
            }
            else if(this.valorDisplay.length >= 6){
                this.tamanhoLetra = 30 + "px";
            }
            else{
                this.tamanhoLetra = 50 + "px";
            }
            // else if(this.valorDisplay.length >= 0){
            //     this.tamanhoLetra = 50 + "px";
            // }
        },//fAjustesTamanhoDisplay
        listarUsuarios(){
            this.usuario = localStorage.getItem ("usuario" || "")
            if(this.usuario === "admin"){
                if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                    this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                    this.senhas = JSON.parse(localStorage.getItem("senhas"));
                }
                this.mostrarLista = !this.mostrarLista;
            }
        },
        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.erro = "Ousuário ADM não pode ser excluído";
                }, 500);
                return;
            }
            if(confirm("Tem certeza que deseja excluir este usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index  !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    //Atualiza o array usuarios no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.erro = null;
                        this.sucesso = "Usuário excluído com sucess";
                    }, 500);
                }
            }
        }
    },
}).mount("#app");