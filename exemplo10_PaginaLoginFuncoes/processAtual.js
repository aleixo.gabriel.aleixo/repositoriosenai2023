const {createApp} = Vue; // Importa a função createApp do Vue.js

createApp({
    data(){
        return{      
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,

            //Arrays (vetores) para armazenamento dos nomes de usuários e senhas
            usuarios: ["admin", "adriano", "julia"],
            senhas:["1234", "1234", "1234"],
            userAdmin: false,
            mostrarEntrada: false,

            //Variáveis para tratamento das informações dos novos usuários
            newUsername: "",
            newEmail: "",
            newPassword: "",
            confirmPassword: "",

            mostrarLista: false,
        };
    },

    methods:{
        login2(){
            this.mostrarEntrada = false;

            setTimeout(() => {
                this.mostrarEntrada = true;

                //Verificação de usuário e senha cadastrados ou não nos arrays
                const index = this.usuarios.indexOf(this.usuario);
                if(index !== -1 && this.senhas[index] === this.senha){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!";

                    //Registrando o usuário no LocalStorage para lembrete de acessos
                    localStorage.setItem("usuario", this.usuario);
                    localStorage.setItem("senha", this.senha);

                    //Verificando se o usuário é admin
                    if(this.usuario === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!";
                    }
                    localStorage.setItem("userAdmin", this.userAdmin);
                }//Fechamento if
                else{
                    this.sucesso = null;
                    this.erro = "Usuário e / ou senha incorretos!";
                }      
            }, 1000);            
        },//Fechamento login2

        paginaCadastro(){
            this.mostrarEntrada = false;
            // if(this.userAdmin == true){
                this.erro = null;
                this.sucesso = "Carregando Página de Cadastro...";
                this.mostrarEntrada = true;

                //Espera estratégica antes do carregamento da página
                setTimeout(() => {}, 2000);

                setTimeout(() => {
                    window.location.href = "cadastro.html";
                }, 1000);                
            // }//Fechamento if
            // else{
            //     this.sucesso = null;
            //     setTimeout(() => {
            //         this.mostrarEntrada = true;
            //         this.erro = "Sem privilégios ADMIN!!!";
            //     }, 1000);
            // }//Fechamento else

        },//Fechamento paginaCadastro

        adicionarUsuario(){
            this.mostrarEntrada = false;            

            this.usuario = localStorage.getItem("usuario");
            this.senha = localStorage.getItem("senha");

            setTimeout(() => {
                this.mostrarEntrada = true;

                if(this.usuario === "admin"){
                    /*Verificando de o novo usuário é diferente de vazio e se ele já não está cadastrado no sistema*/
                    if(!this.usuarios.includes(this.newUsername) &&
                    this.newUsername !== "" && !this.newUsername.includes(" ")){
                        //Validação da senha digitada para cadastro no array
                        if(this.newPassword !== "" && !this.newPassword.includes(" ") && this.newPassword === this.confirmPassword){
                            //Inserindo o novo usuário e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);
                            this.email.push(this.newEmail);

                            //Atualizando o usuário recém cadastrado no LocalStorage
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));
                            
                            this.newUsername = "";
                            this.newEmail = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                            

                            this.erro = null;
                            this.sucesso = "Usuário cadastrado com sucesso!";
                        }//Fechamento if password
                        else{
                            this.sucesso = null;
                            this.erro = "Por favor, informe uma senha válida!!!";

                            this.newUsername = "";
                            this.newEmail = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                            
                        }//FEchamento else
                    }//Fechamento if includes
                    else{
                        this.erro = "Usuário inválido! Por favor digite um usuário diferente!";
                        this.sucesso = null;

                        this.newUsername = "";
                        this.newEmail = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                        
                    }//Fechamento else

                }//Fechamento if admin
                else{
                    this.erro = "Não está logado como ADMIN!";
                    this.sucesso = null;

                    this.newUsername = "";
                    this.newEmail = "";
                    this.newPassword = "";
                    this.confirmPassword = "";
                }//Fechamento else admin
            }, 300);

        },//Fechamento adicionarUsuario

        listarUsuarios(){
            this.usuario = localStorage.getItem("usuario" || "");
            if(this.usuario === "admin"){
                if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                    this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                    this.senhas = JSON.parse(localStorage.getItem("senhas"));                
                }//Fechamento if
                this.mostrarLista = !this.mostrarLista;
            }//Fechamento if
        },//Fechamento listarUsuarios

        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.erro = "O usuário ADMIN não pode ser excluído!!!";                    
                }, 500);
                return; // Força a saída deste bloco
            }//fechamento do if

            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index,1);

                    //Atualiza o array usuarios no Localstorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));

                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.erro = null;
                        this.sucesso = "Usuário excluído com sucesso!";
                    }, 500);
                }//Fechamento if index
            }//Fechamento if
        },//Fechamento excluirUsuario

    },//Fechamento methods

}).mount("#app"); // Monta a aplicação Vue.js no elemento HTML com o ID "app"