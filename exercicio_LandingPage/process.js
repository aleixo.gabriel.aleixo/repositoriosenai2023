const {createApp} = Vue;

createApp({
    data(){
        return{
            mensagem: "Bem vindo(a) ao SiteSemNome",
        };//fReturn
    },//fData
}).mount("#home");//fApp

const openModalBtn = document.getElementById('openModalBtn');
const loginModal = document.getElementById('loginModal');
const closeModal = document.querySelector('.close');

// Abrir a janela modal quando o botão é clicado
openModalBtn.addEventListener('click', () => {
  loginModal.style.display = 'block';
});

// Fechar a janela modal quando o botão de fechar é clicado
closeModal.addEventListener('click', () => {
  loginModal.style.display = 'none';
});

// Fechar a janela modal quando o usuário clicar fora dela
window.addEventListener('click', (event) => {
  if (event.target == loginModal) {
    loginModal.style.display = 'none';
  }
});
