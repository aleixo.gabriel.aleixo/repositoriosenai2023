const {createApp} = Vue;
createApp({
    data(){
        return{
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        }//fReturn
    },//fData
    methods:{
        login(){
            // alert("testando...");
            //Simulando Uma Requisição De Login Assicrona
            setTimeout(() => {
                if((this.usuario === "GaAleixo" && this.senha === "senhaSegura") || 
                (this.usuario === "nome" && this.senha === "senha") ||
                (this.usuario === "bomDia" && this.senha === "boaNoite") ||
                (this.usuario === "abacate" && this.senha === "vitamina")
                ){
                    this.erro = null
                    this.sucesso = "Login efetuado com sucesso!";
                    // alert("Login efetuado com sucesso!")
                }//fIf
                else{
                    // alert("Usuário ou Senha incorretos!")
                    this.erro = "Usuário ou Senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);
        },//fLogin
    },//fMethods
}).mount("#app");//fApp